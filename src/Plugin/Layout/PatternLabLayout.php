<?php

namespace Drupal\layouts_pattern_lab\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;

/**
* A layout from our flexible layout builder.
*
* @Layout(
*   id = "pattern_lab_layout",
*   deriver = "Drupal\layouts_pattern_lab\Plugin\Deriver\PatternLabLayoutDeriver"
* )
*/
class PatternLabLayout extends LayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
        'field_templates' => 'default',
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $form['field_templates'] = [
      '#type' => 'select',
      '#title' => $this->t('Templates'),
      '#options' => [
        'default' => $this->t("Default"),
        'only_content' => $this->t("Only content"),
      ],
      '#description' => implode('<br/>', [
        $this->t("<b>Default</b>: use block and field templates to wrap content."),
        $this->t("<b>Only content</b>: only print content, without block or field wrapping. Cleaner markup on rendered page, but currently disables some layout builder functionality."),
      ]),
      '#default_value' => $configuration['field_templates'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // any additional form validation that is required
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['field_templates'] = $form_state->getValue('field_templates');
  }

  /**
  * {@inheritdoc}
  */
  public function build(array $regions) {
    //$render_array = parent::build($regions);

    $configuration = $this->getConfiguration();

    // Remove default field template if "Only content" option has been selected.
    if ($configuration['field_templates'] == 'only_content') {
      $this->processOnlyContent($regions);
    }

    // We could use a similar approach to above in order to re-insert attributes
    // and otherwise retain additional Layout Builder functionality

    return parent::build($regions);
  }

  /**
   * Remove default block and field templates if "Only content" option has been selected.
   * Based on approach in the excellent ui_patterns module
   *
   * @param array $regions
   *   Layout regions.
   */
  protected function processOnlyContent(array &$regions) {
    foreach ($regions as $region_name => $region) {
      if (is_array($region)) {
        foreach ($regions[$region_name] as $field_name => $field) {
          if (is_array($field) && isset($field['#theme']) && $field['#theme'] == 'block') {
            $regions[$region_name][$field_name]['#theme'] = NULL;
            if (isset($field['content']['#theme']) && $field['content']['#theme'] == 'field') {
              $regions[$region_name][$field_name]['content']['#theme'] = NULL;
            }
          }
        }
      }
    }
  }

}
