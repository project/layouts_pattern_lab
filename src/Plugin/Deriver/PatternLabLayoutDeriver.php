<?php

namespace Drupal\layouts_pattern_lab\Plugin\Deriver;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Layout\LayoutDefinition;
use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Serialization\Json;
use Drupal\layouts_pattern_lab\Plugin\Layout\PatternLabLayout;
use Spatie\YamlFrontMatter\YamlFrontMatter;

/**
 * Makes a flexible layout for each layout config entity.
 */
class PatternLabLayoutDeriver extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getFileExtensions() {
    // Configuration files can be formatted as JSON or YAML
    return [
      ".json",
      ".yml",
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function fileScanDirectory($directory) {
    // TODO - Remove tests directory
    // $options = ['nomask' => $this->getNoMask()];
    $options = [];
    $extensions = $this->getFileExtensions();
    $extensions = array_map('preg_quote', $extensions);
    $extensions = implode('|', $extensions);
    return \Drupal::service('file_system')->scanDirectory($directory, "/{$extensions}$/", $options, 0);
  }

  /**
   * {@inheritdoc}
   */
  public function getRegions($content) {
    $regions = [];

    // TODO - Allow regions to be explicitly defined/overriden in pattern files

    // Cross our fingers and define regions based on fields in the definition file
    foreach ($content as $region => $preview) {
      // TODO - Ignore Drupal layout override
      $regions[$region] = [
        "label" => ucwords($region),
      ];

      // TODO - Support data transform things? Include(), join(), attribute() and url()

    }

    // TODO - do we need to ignore / unset the attributes region here if included?

    return $regions;
  }

  /**
   * {@inheritdoc}
   */
  public function getPatterns() {
    $patterns = [];
    $themes = [];
    $root = \Drupal::root();

    // TODO - traverse active modules for pattern data
    // How common is this really?

    // Get the list of currently active default theme and related base themes
    $theme_handler = \Drupal::service('theme_handler');
    $default_theme = $theme_handler->getDefault();
    $themes[$default_theme] = $default_theme;
    $base_themes = $theme_handler->getBaseThemes($theme_handler->listInfo(), $default_theme);
    $themes = $themes + $base_themes;

    // TODO - Consolidate this with the logic above
    // Get a list of the path to /templates in all active themes
    $active_directories = $theme_handler->getThemeDirectories();
    foreach ($active_directories as $provider => $path) {
      if (array_key_exists($provider, $themes)) {
        $active_directories[$provider] = $path . "/templates";
      }
      else {
        unset($active_directories[$provider]);
      }
    }

    // Determine the paths to any defined component libraries.
    $namespace_paths = [];
    foreach ($themes as $theme => $item) {
      $theme_config = $theme_handler->getTheme($theme);
      if (isset($theme_config->info["component-libraries"])) {
        foreach ($theme_config->info["component-libraries"] as $namespace => $path) {
          foreach($path['paths'] as $key => $path_item) {
            $subpath = $theme_config->getPath();
            $namespace_path = $path_item;
            $provider = $theme . "@" . $namespace . "_" . $key;
            $namespace_paths[$provider] =  $root . "/" . $subpath . "/" . $namespace_path;
          }
        }
      }
    }

    // Combine theme and component library paths
    $all_directories = $active_directories + $namespace_paths;

    // Traverse directories looking for pattern definitions
    foreach ($all_directories as $provider => $directory) {
      foreach ($this->fileScanDirectory($directory) as $file_path => $file) {
        $absolute_base_path = dirname($file_path);
        $base_path = str_replace($root, "", $absolute_base_path);
        $id = $file->name;
        $definition = [];

        // Parse definition file.
        $content = [];
        if (preg_match('/\.yml$/', $file_path)) {
          $content = file_get_contents($file_path);
          $content = Yaml::decode($content);
        }
        elseif (preg_match('/\.json$/', $file_path)) {
          $content = file_get_contents($file_path);
          $content = Json::decode($content);
        }

        // Pattern definition needs to have some valid content
        if (empty($content)) {
          continue;
        }

        // We need a Twig file to have a valid pattern.
        if (!$this->templateExists($content, $absolute_base_path, $id)) {
          continue;
        }

        // For initial implementation, make pattern discovery opt-in
        if (!isset($content['drupal']['layouts_pattern_lab']['discover']) || $content['drupal']['layouts_pattern_lab']['discover'] != 'true') {
          continue;
        }

        // Parse markdown documentation if it exists
        $documentation = $this->getDocumentation($absolute_base_path, $id);

        // Set Layout Definition

        // Required:
        // ID
        // Convert hyphens to underscores so that the pattern id will validate.
        // Also strip initial numbers that are ignored by Pattern Lab when naming.
        // TODO - Strip double underscores?
        $definition['id'] = ltrim(str_replace("-", "_", $file->name), "0..9_");

        // Label
        $definition['label'] = $this->getLabel($content, $documentation, $definition['id']);

        // Category
        // If pattern is provided by a twig namespace, pass just the theme name
        // as the provider
        $provider_array = explode("@", $provider);
        $definition['category'] = array_shift($provider_array);

        // Regions
        $definition['regions'] = $this->getRegions($content);

        // Template or Theme
        // TODO - protect from missing templates - disable or fall back on core layout.
        $definition['template'] = $this->getTemplatePath($content, $base_path, $absolute_base_path, $id);

        // Optional:
        // default_region
        // icon_map
        // description - get from documentation
        // path
        // library
        // provider

        // Set other pattern values.
        // TODO - Handle Libraries

        // Add pattern to collection.
        $patterns[] = $definition;

      }
    }

    // TODO - Create new config entities
    // Disable any config entities for disabled themes.

    return $patterns;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    $patterns = $this->getPatterns();

    // TODO - strip all patterns down to list enabled in config.

    // Create layout definition for all patterns
    foreach ($patterns as $pattern) {
      // Here we fill in any missing keys on the layout annotation.
      $this->derivatives[$pattern['id']] = new LayoutDefinition([
        'class' => PatternLabLayout::class,
        'label' => $pattern['label'],
        'category' => $pattern['category'],
        'template' => $pattern['template'],
        'regions' => $pattern['regions'],
      ]);
    }

    return $this->derivatives;
  }

  /**
   * {@inheritdoc}
   */
  private function templateExists($content, $absolute_base_path, $id) {
    // If a Twig template is explicitly defined, use that...
    if (isset($content['drupal']['layouts_pattern_lab']['template'])) {
      // Strip out only the file name in case a path was provided in the use value
      $template_array = explode("/", $content['drupal']['layouts_pattern_lab']['template']);
      $template_file = end($template_array);
      return file_exists($absolute_base_path . "/" . $template_file) ? TRUE : FALSE;
    }
    // Otherwise look for a template that contains the same name as the pattern deifnition file.
    else {
      $glob_paths = glob($absolute_base_path . "/*" . ltrim($id, "0..9_-") . ".twig");
      $closest_template = array_shift($glob_paths);
      if ($closest_template != NULL) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  private function getLabel($content, $documentation, $id) {
    // If the label was manually overriden, use that.
    if (isset($content['drupal']['layouts_pattern_lab']['label'])) {
      return $content['drupal']['layouts_pattern_lab']['label'];
    }
    // If a title is included in documentaton frontmatter, use that.
    elseif ($documentation->matter('title') !== NULL) {
      return $documentation->matter('title');
    }
    // Otherwise, fall back on a cleaned up version of the id
    else {
      return ucwords(urldecode($id));
    }
  }

  /**
   * {@inheritdoc}
   */
  private function getTemplatePath($content, $base_path, $absolute_base_path, $id) {
    // If a Twig template is explicitly defined, use that...
    if ($content['drupal']['layouts_pattern_lab']['template']) {
      // Strip out only the file name in case a path was provided in the use value
      $template_array = explode("/", $content['drupal']['layouts_pattern_lab']['template']);
      $template_file = end($template_array);
      $template_file = str_replace(".html","", $template_file);
      $template_file = str_replace(".twig","", $template_file);
      return ltrim($base_path, '/') . "/" . $template_file;
    }
    // Next try an exact match for a template with the same name as the
    // pattern deifnition file.
    elseif (file_exists($absolute_base_path . "/" . $id . ".twig")) {
      return ltrim($base_path, '/') . "/" . $id;
    }
    // Finally, look for a match that contains the id. This allows for a template
    // name that only differs by leading numbers for example.
    else {
      // Assuming here that the first match is our best option.
      $glob_paths = glob($absolute_base_path . "/*" . ltrim($id, "0..9_-") . ".twig");
      $closest_template = array_shift($glob_paths);
      $template_file = str_replace($absolute_base_path, ltrim($base_path, '/'), $closest_template);
      $template_file = str_replace(".html","", $template_file);
      $template_file = str_replace(".twig","", $template_file);
      return $template_file;
    }
  }

  /**
   * {@inheritdoc}
   */
  private function getDocumentation($absolute_base_path, $id) {
    $glob_paths = glob($absolute_base_path . "/*" . ltrim($id, "0..9_-") . ".md");
    $closest_md = array_shift($glob_paths);
    // Try an exact match for a markdown file with the same name as the
    // pattern definition file.
    if (file_exists($absolute_base_path . "/" . $id . ".md")) {
      $md = file_get_contents($absolute_base_path . "/" . $id . ".md");
      return YamlFrontMatter::parse($md);
    }
    // Otherwise, look for a match that contains the id. This allows for a markdown
    // name that only differs by leading numbers for example.
    elseif ($closest_md != NULL) {
      // Assuming here that the first match is our best option.
      $md = file_get_contents($closest_md);
      return YamlFrontMatter::parse($md);
    }
    // If we can't find any .md file return an empty YamlFrontMatter object.
    else {
      return YamlFrontMatter::parse("");
    }
  }

}
